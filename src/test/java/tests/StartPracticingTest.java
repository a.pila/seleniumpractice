package tests;

import com.gargoylesoftware.htmlunit.Page;
import cucumber.api.java.hu.De;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.BeforeTest;
import pages.CheckListOfContents;
import pages.StartPracticingPage;
import ro.dataprovider.ConfigFileReader;
import ro.dataprovider.LoginFactory;
import utilitiesLibrary.Description;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class StartPracticingTest {

    private StartPracticingPage iStartPracticingPage;
    private LoginFactory iLoginFactory;
    private ConfigFileReader configFileReader;

    public StartPracticingTest() {
        configFileReader = new ConfigFileReader ();
        iLoginFactory = LoginFactory.getInstanceOfLoginFactory ();
    }

    private By aElements = By.xpath ("//a[@class='list-group-item']");


    @Test()
    @Description ( name = "click Start Practicing", description = "click Start Practicing")
    public void navigateToStartPracticing() {
        iLoginFactory.initListener ();
        iLoginFactory.openMaximizeWindow( configFileReader );
        iStartPracticingPage = PageFactory.initElements ( iLoginFactory.getDriver (), StartPracticingPage.class );
        iStartPracticingPage.startButtonElement.click ();
    System.out.println ( "========>>>>>>>  Clicked on button." );
    }

    @Test()
    @Description ( name = "check list of contents", description = "check list of contents")
    public void verifyList() {

        iLoginFactory.initListener ();
        iLoginFactory.openMaximizeWindow( configFileReader );
        iStartPracticingPage = PageFactory.initElements ( iLoginFactory.getDriver (), StartPracticingPage.class );
        iStartPracticingPage.startButtonElement.click ();
        System.out.println ( "========>>>>>>>  Clicked on button." );

        WebDriver iDriver = iLoginFactory.getDriver ();
        // collect all elements as Web elements from the site
        WebElement a = iDriver.findElement (aElements);

        // convert the List of web elements in a list of Strings
        List<WebElement> actualWebList = a.findElements ( By.tagName ("a") );

        // declare the expected list elements
        List<String> actualStringList = new LinkedList<> (  );

        for (WebElement e : actualWebList) {
            actualStringList.add ( e.getText () );
        }

        List iExpectedList = new ArrayList (  );
        iExpectedList.add ( "Simple Form Demo" );
        iExpectedList.add ( "Check Box Demo" );
        iExpectedList.add ( "Radio Buttons Demo" );
        iExpectedList.add ( "Select Dropdown List" );
        iExpectedList.add ( "Javascript Alerts" );
        iExpectedList.add ( "Window Popup Modal" );
        iExpectedList.add ( "Bootstrap Alerts" );
        iExpectedList.add ( "Bootstrap Modals" );

        for (int i = 0; i < iExpectedList.size (); i++) {

            if (actualStringList.contains ( iExpectedList.get ( i ) )) {
                System.out.println ( "The menu item is listed correct =>   " + iExpectedList.get ( i ) );
            } else {
                System.out.println ( "The menu item is <not> listed correct =>   " + iExpectedList.get ( i ) );
            }
        }
    }



}
